# HD-STRAPI

happydev montpellier strapi instance

## Local Development

- Requires docker
- cd into the root directory of this project and execute

(docker rm -f hd_strapi || true); \
docker run -d \
  --name hd_strapi \
  -e NODE_ENV=development \
  -e DATABASE_CLIENT=mongo \
  -e DATABASE_NAME=hd_strapi \
  -e DATABASE_HOST=178.128.254.49 \
  -e DATABASE_PORT=27017 \
  -e DATABASE_USERNAME=XXX \
  -e DATABASE_PASSWORD=XXX \
  -v $(pwd)/app:/srv/app \
  -p 1337:1337 \
  strapi/strapi
  docker logs -f hd_strapi

- Navigate to localhost:1337

## Deployment

```sh
#1337 (strapi:1337 for caddy)
cd /root/git/hd_strapi; \
(docker rm -f strapi || true); \
docker run -d \
  --name hd_strapi \
  -e NODE_ENV=production \
  -e DATABASE_CLIENT=mongo \
  -e DATABASE_NAME=strapi \
  -e DATABASE_HOST=host \
  -e DATABASE_PORT=27017 \
  -e DATABASE_USERNAME=xxxx \
  -e DATABASE_PASSWORD=xxxx \
  -v $(pwd)/app:/srv/app \
  --net=caddy-node_caddy --net-alias=strapi \
  strapi/strapi strapi start
````

